#ifndef COCOS2DX_H
#define COCOS2DX_H

#include "cocos2dx_global.h"

#include <extensionsystem/iplugin.h>

namespace Cocos2dX {
namespace Internal {

class Cocos2dXPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QtCreatorPlugin" FILE "Cocos2dX.json")

public:
    Cocos2dXPlugin();
    ~Cocos2dXPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

private slots:
    void triggerAction();
};

} // namespace Internal
} // namespace Cocos2dX

#endif // COCOS2DX_H

