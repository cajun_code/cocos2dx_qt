#ifndef COCOS2DXCONSTANTS_H
#define COCOS2DXCONSTANTS_H

namespace Cocos2dX {
namespace Constants {

const char ACTION_ID[] = "Cocos2dX.Action";
const char MENU_ID[] = "Cocos2dX.Menu";

} // namespace Cocos2dX
} // namespace Constants

#endif // COCOS2DXCONSTANTS_H

